require 'rest-client'
require 'json'

$debug = 0

class FMoney

  def initialize(source)
    @source = source
    @api = 'http://www.apilayer.net/api/'
    @urllive = 'live?'
    @urlhistorical = 'historical?'
    @key = 'access_key=2e3d98fc1469e7d0349e81fd5e1cd129'
  end
  
  def source()
    exchange_source()
  end
  def recent(target)
    command = @api + @urllive + @key
    exchange_recent(target,command)
  end

  def date(target,date)
    command = @api + @urlhistorical + @key + '&date=' + date
    exchange_recent(target,command)
  end

  def convert(target,amount)
    command = @api + @urllive + @key
    exchange_convert(target,amount,command)
  end

  def best(target)
    exchange_best(target)
  end

  private

  def exchange_recent(targets,command)
    includusds = []
    usdresults = []
    dones = []

    targets.each_with_index do |target, i|
      includusds[i] = 'USD' + target
    end 

    isource = 'USD' + @source
    begin
      response = RestClient.get(command)
    rescue => e
      print e if $debug 
      print "error" if $debug 
      dones = nil
      return
    end
    json = JSON.parse(response)
    if json['success'] == true
      includusds.each_with_index do |includusd, i|
        usdresults[i] = json['quotes'][includusd];
        usdratio = json['quotes'][isource]; 
        dones[i] = usdresults[i]/usdratio
      end
    else
      print json['error'] if $debug 
      print "\n" if $debug
      dones = json['error'] 
    end 
    dones 
  end

  def exchange_convert(target,amount,command)
    includusd = 'USD' + target
    isource = 'USD' + @source
    begin
      response = RestClient.get(command)
      print response if $debug 
      print "\n" if $debug
      return
    rescue => e
      print e if $debug 
      print "error" if $debug 
      dones = nil
    end
    json = JSON.parse(response)
    if json['success'] == true
      usdresult = json['quotes'][includusd]
      usdratio = json['quotes'][isource] 
      dones = usdresult / usdratio * amount 
    else
      print json['error'] if $debug 
      print "\n" if $debug
      dones = json['error'] 
    end 
  end
  
  def exchange_best(target)
    results = [] 
    day = [] 
   
    (7).downto(0) do |i| 
      day[i] = (DateTime.now - i).strftime("%Y-%m-%d")
      results[i] = date([target], day[i])
      print day[i] if $debug
      print results[i] if $debug
      print "\n" if $debug
    end
    results
    #best = results.max[0]
  end

end
